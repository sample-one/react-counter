import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      message: '',
    };
  }

  componentDidMount() {
    this.setState({ message: 'Welcome to the counter app!' });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count) {
      this.setState({ message: `The count is now ${this.state.count}` });
    }
  }

  incrementHandler = () => {
    this.setState((prevState) => {
      return { count: prevState.count + 1 };
    });
  };

  decrementHandler = () => {
    this.setState((prevState) => {
      return { count: prevState.count - 1 };
    });
  };

  resetHandler = () => {
    this.setState({ count: 0 });
  };

  render() {
    return (
      <div className="count">
        <h2>Counter</h2>
        <h1>{this.state.count}</h1>
        <div className="message">{this.state.message}</div>
        <div className="action-btn">
          <button onClick={this.incrementHandler}>Increment</button>
          <button onClick={this.decrementHandler}>Decrement</button>
          <button onClick={this.resetHandler}>Reset</button>
        </div>
      </div>
    );
  }
}

export default App;
